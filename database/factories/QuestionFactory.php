<?php

namespace Database\Factories;

use App\Models\Genre;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuestionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
            'genre_id' => Genre::all()->random()->id,
            'animetitle' => $this->faker->name,
            'created_at' => Carbon::now()->setTimezone('Europe/Amsterdam'),
            'updated_at' => Carbon::now()->setTimezone('Europe/Amsterdam'),
        ];
    }
}

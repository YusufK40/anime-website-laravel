<?php

namespace Database\Factories;

use App\Models\Genre;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class AnimemovieFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'moviename' => $this->faker->name,
            'summary' => $this->faker->text,
            'genre_id' => Genre::all()->random()->id,
            'releasedate' => $this->faker->year(),
            'created_at' => Carbon::now()->setTimeZone('Europe/Amsterdam'),
            'updated_at' => Carbon::now()->setTimeZone('Europe/Amsterdam'),
        ];
    }
}

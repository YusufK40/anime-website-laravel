<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class GenreFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'genrename'=> $this->faker->name,
            'created_at' => Carbon::now()->setTimezone('Europe/Amsterdam'),
            'updated_at' => Carbon::now()->setTimezone('Europe/Amsterdam'),
        ];
    }
}

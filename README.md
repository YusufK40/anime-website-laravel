# Anime Website - Laravel

Anime Website Project, gemaakt door Yusuf Koc en Emre Azikci.


Mijn bijdrage aan dit project:

- Een basis-layout voor de Admin
- Genre CRUD aangemaakt
- Season CRUD aangemaakt
- Animeserie CRUD aangemaakt
- Na het inloggen verwijzen naar startpagina
- Relatie maken tussen Animeserie en Genre
- Relatie maken tussen Animeserie en Season
- Een Gebruiker moet een Question kunnen indienen
- Testen Genre CRUD met Dusk
- Testen Season CRUD met Dusk
- Testen Animeserie CRUD met Dusk
- Feature en Unit testen Genre
- Feature en Unit testen Season
- Feature en Unit testen Animeserie

<?php

use App\Models\Animeserie;
use App\Models\Genre;
use App\Models\Season;
use Carbon\Carbon;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->season = Season::factory()->create();
    $this->animeserie = Animeserie::factory()->create();
});

test('a anime id is a int', function (){
    
    expect($this->animeserie->id)->toBeInt();
})->group('AnimeserieUnit');

test('a anime has a genre', function (){
    expect($this->animeserie->genre)->toBeInstanceOf(Genre::class);
})->group('AnimeserieUnit');

test('a anime has a season', function (){
    expect($this->animeserie->season)->toBeInstanceOf(Season::class);
})->group('AnimeserieUnit');

test('a animeserie is a string', function (){
    expect($this->animeserie->seriename)->toBeString();
})->group('AnimeserieUnit');

test('a summary is a string', function (){
    expect($this->animeserie->summary)->toBeString();
})->group('AnimeserieUnit');

test('a releasedate is a int', function (){
    expect($this->animeserie->releasedate)->toBeNumeric();
})->group('AnimeserieUnit');

test('created_at is a type of Carbon', function (){
    expect($this->animeserie->created_at)->toBeInstanceOf(Carbon::class);
})->group('AnimeserieUnit');

test('updated_at is a type of Carbon', function (){
    expect($this->animeserie->updated_at)->toBeInstanceOf(Carbon::class);
})->group('AnimeserieUnit');

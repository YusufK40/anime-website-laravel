<?php

namespace Tests\Feature\Question;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use App\Models\Genre;
use App\Models\Question;
use App\Models\User;


class QuestionStoreCheckTest extends TestCase
{
    use DatabaseMigrations;

    public function setup(): void
    {
        parent::setup();

        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');
        $this->genre = Genre::factory()->create();
        $this->question = Question::factory()->create();
    }

    public function postQuestion($overrides = [])
    {
        $question = Question::factory()->make($overrides);
        $genre = Genre::factory()->make();

        return $this->postJson(route('question.store'), array_merge($question->toArray(), $genre->toArray()));
    }

    /** @test 
     *  @group QuestionStoreCheck
     */
    function a_question_requires_a_animetitle()
    {
        $admin = User::find(2);
        $this->actingAs($admin);
        $this->postQuestion(['animetitle' => null])->assertStatus(422);
    }

    /** @test 
     *  @group QuestionStoreCheck
     */
    function a_question_can_be_max_65_characters()
    {
        $admin = User::find(2);
        $this->actingAs($admin);
        $this->postQuestion(['animetitle' => '123456789012345678901234567890123456789012345678901234567890123456'])->assertStatus(422);
    }
}

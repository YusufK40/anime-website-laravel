<?php

use App\Models\Question;
use App\Models\Genre;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->question = Question::factory()->create();
});

test('admin can delete a question', function () {
    $admin = User::find(2);
    Laravel\be($admin);
    $this->json('DELETE', route('question.destroy', ['question' => $this->question->id]));
    $this->assertDatabaseMissing('questions', ['id' => $this->question->id]);
    // $this->assertDatabaseMissing('genres', ['id' => $this->genre->id]);

})->group('QuestionDestroy');

test('user cannot delete a question', function () {
    $admin = User::find(1);
    Laravel\be($admin);
    $this->json('DELETE', route('question.destroy', ['question' => $this->question->id]))
    ->assertForbidden();

})->group('QuestionDestroy');

test('guest cannot delete a serie', function () {
    $this->json('DELETE', route('question.destroy', ['question' => $this->question->id]))
    ->assertStatus(401);

})->group('QuestionDestroy');
<?php

use App\Models\Question;
use App\Models\Genre;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function () {
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->question = Question::factory()->create();
});

test('admin can create a question', function () {
    $this->withoutExceptionHandling();
    $admin = User::find(2);
    $question = Question::factory()->make([
        'animetitle' => 'One Piece',
        'genre_id' => $this->genre->id
    ]);

    Laravel\be($admin)
        ->postJson(route('question.store'), $question->toArray())
        ->assertRedirect(route('open.question.create'));

    $this->assertDatabaseHas('questions', [
        'animetitle' => 'One Piece',
        'genre_id' => 1,
    ]);

})->group('QuestionStore');

test('user can create an question', function () {
    $user = User::find(1);
    $question = Question::factory()->make([
        'animetitle' => 'One Piece',
        'genre_id' => $this->genre->id
    ]);

    Laravel\be($user)
        ->postJson(route('question.store'), $question->toArray())
        ->assertRedirect(route('open.question.create'));

    $this->assertDatabaseHas('questions', [
        'animetitle' => 'One Piece',
        'genre_id' => 1
    ]);
    
})->group('QuestionStore');


test('guest cannot create a question', function () {
    $this->postJson(route('question.store'))
        ->assertStatus(401);

})->group('QuestionStore');
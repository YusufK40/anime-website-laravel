<?php

use App\Models\Question;
use App\Models\Genre;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->user = User::factory()->create();
    $this->genre = Genre::factory()->create();
    $this->question = Question::factory()->create();
});

test('admin can see question show page', function () {
    $admin = User::find(2);
    Laravel\be($admin)
    ->get(route('question.show', ['question' => $this->question->id]))
    ->assertViewIs('admin.question.show')
    ->assertSee($this->question->user->id)
    ->assertSee($this->question->id)
    ->assertSee($this->question->user->name)
    ->assertSee($this->question->animetitle)
    ->assertSee($this->question->genre->genrename)
    ->assertSee($this->question->created_at)
    ->assertStatus(200);

})->group('QuestionShow');


test('user cannot see question show page', function () {
    $user = User::find(1);
    Laravel\be($user)
    ->get(route('question.show', ['question' => $this->question->id]))
    ->assertForbidden();

})->group('QuestionShow');


test('guest cannot see question show page', function () {
    $this->get(route('question.show' , ['question' => $this->question->id]))
        ->assertRedirect(route('login'));
       
})->group('QuestionShow');
<?php

use App\Models\Question;
use App\Models\Genre;
use App\Models\User;
use \Pest\Laravel;

beforeEach(function (){
    $this->seed('RoleAndPermissionSeeder');
    $this->seed('UserSeeder');
    $this->genre = Genre::factory()->create();
    $this->question = Question::factory()->create();
});

test('admin can see question delete page', function () {
    $admin = User::find(2);
    Laravel\be($admin)
    ->get(route('question.delete', ['question' => $this->question->id]))
    ->assertViewIs('admin.question.delete')
    ->assertSee($this->question->animetitle)
    ->assertSee($this->genre->genrename)
    ->assertStatus(200);

})->group('QuestionDelete');

test('user cannot see question delete page', function () {
    $user = User::find(1);
    Laravel\be($user)
    ->get(route('question.delete', ['question' => $this->question->id]))
    ->assertForbidden();

})->group('QuestionDelete');

test('guest cannot see question delete page', function () {
    $this->get(route('question.delete' , ['question' => $this->question->id]))
        ->assertRedirect(route('login'));
        
})->group('QuestionDelete'); 
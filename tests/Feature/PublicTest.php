<?php

it('has a public page', function () {
    $response = $this->get('/');
    $response->assertStatus(200);
})->group('PublicTest');
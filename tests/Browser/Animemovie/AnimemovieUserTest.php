<?php

namespace Tests\Browser;

use App\Models\Animemovie;
use App\Models\Genre;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class AnimemovieUserTest extends DuskTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function can_view_index_of_animemovie()
    {
        $animemovie = Animemovie::factory([
            'genre_id' => Genre::factory()->create()->id,
        ])->create();


        $this->browse(function (Browser $browser) use ($animemovie) {
            $browser->visit(route('open.animemovie.index'))
                ->assertSee('Populair Movies')
                ->assertSee($animemovie->moviename)
                ->assertSee('Episode 1'); 
        });
    }

    /** @test */
    public function can_view_show_of_animemovie()
    {
        $animemovie = Animemovie::factory([
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animemovie) {
            $browser->visit(route('open.animemovie.show', $animemovie))
                ->assertSee('Anime Info')
                ->assertSee('Summary: '.$animemovie->summary)
                ->assertSee('Genre: '.$animemovie->genre->genrename)
                ->assertSee('Released: '.$animemovie->releasedate)
                ->assertSee('Episodes');
        });
    }

    /** @test */
    public function user_can_not_create_a_animemovie()
    {
        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');

        $animemovie = Animemovie::factory([
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animemovie) {
            $browser->loginAs(User::find(1))
                ->visit(route('animemovie.create'))
                ->assertSee('USER DOES NOT HAVE THE RIGHT ROLES.');
        });
    }

    /** @test */
    public function user_can_not_edit_a_animemovie()
    {
        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');

        $animemovie = Animemovie::factory([
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animemovie) {
            $browser->loginAs(User::find(1))
                ->visit('/admin/animemovie/1/edit')
                ->assertSee('USER DOES NOT HAVE THE RIGHT ROLES.');
        });
    }

    /** @test */
    public function user_can_not_delete_a_animemovie()
    {
        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');

        $animemovie = Animemovie::factory([
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animemovie) {
            $browser->loginAs(User::find(1))
                ->visit('/admin/animemovie/1/delete')
                ->assertSee('USER DOES NOT HAVE THE RIGHT ROLES.');
        });
    }
}

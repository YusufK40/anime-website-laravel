<?php

namespace Tests\Browser\Animemovie;

use App\Models\Animemovie;
use App\Models\Genre;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class AnimemovieTest extends DuskTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function admin_can_view_index_of_animemovie()
    {
        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');

        $animemovie = Animemovie::factory([
            'genre_id' => Genre::factory()->create()->id,
        ])->create();


        $this->browse(function (Browser $browser) use ($animemovie) {
            $browser->loginAs(User::find(2))
                ->visit(route('animemovie.index'))
                ->assertSee($animemovie->id)
                ->assertSee($animemovie->moviename)
                ->assertSee($animemovie->summary)
                ->assertSee($animemovie->releasedate)
                ->assertSee($animemovie->genre->genrename)
                ->assertSee('Edit')
                ->assertSee('Delete')
                ->assertSee('Add +');
        });
    }

    /** @test */
    public function admin_can_create_a_animemovie()
    {

        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');

        $animemovie = Animemovie::factory([
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animemovie) {
            $browser->loginAs(User::find(2))
                ->visit(route('animemovie.index'))
                ->clickLink('Add +')
                ->type('moviename', 'One Piece')
                ->type('summary', 'asddddddddddwwddsdadd')
                ->type('releasedate', '3434')
                ->select('genre_id')
                ->press('Add')
                ->assertSee('Succesfully added a new Animemovie!');
        });
    }

    /** @test */
    public function admin_can_edit_a_animemovie()
    {

        $this->seed('RoleAndPermissionSeeder');
        $this->seed('UserSeeder');

        $animemovie = Animemovie::factory([
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) use ($animemovie) {
            $browser->loginAs(User::find(2))
                ->visit(route('animemovie.index'))
                ->clickLink('Edit')
                ->type('moviename', 'Pokemon')
                ->type('summary', 'zxzxsdsdzxwd')
                ->type('releasedate', '1123')
                ->select('genre_id')
                ->press('Edit')
                ->assertSee('Succesfully updated this Animemovie!');
        });
    } 

        /** @test */
        public function admin_can_delete_a_animemovie()
        {
    
            $this->seed('RoleAndPermissionSeeder');
            $this->seed('UserSeeder');
    
            $animemovie = Animemovie::factory([
                'genre_id' => Genre::factory()->create()->id,
            ])->create();
    
            $this->browse(function (Browser $browser) use ($animemovie) {
                $browser->loginAs(User::find(2))
                ->visit(route('animemovie.index'))
                ->clickLink('Delete')
                ->press('Delete')
                ->assertSee('This Animemovie was deleted succesfully!');
            });
        } 
}
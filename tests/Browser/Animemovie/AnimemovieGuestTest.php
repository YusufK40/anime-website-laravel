<?php

namespace Tests\Browser\Animemovie;

use App\Models\Animemovie;
use App\Models\Genre;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class AnimemovieGuestTest extends DuskTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function guest_can_not_view_create_of_animemovie()
    {

        $this->browse(function (Browser $browser) {
            $browser->visit(route('animemovie.create'))
            ->assertPathIs('/login');
        });
    }

    /** @test */
    public function guest_can_not_view_edit_of_animemovie()
    {

        $animemovie = Animemovie::factory([
            'genre_id' => Genre::factory()->create()->id,
        ])->create();

        $this->browse(function (Browser $browser) {
            $browser->visit('/admin/animemovie/1/edit')
            ->assertPathIs('/login');
        });
    }

        /** @test */
        public function guest_can_not_view_delete_of_animemovie()
        {
    
            $animemovie = Animemovie::factory([
                'genre_id' => Genre::factory()->create()->id,
            ])->create();
    
            $this->browse(function (Browser $browser) {
                $browser->visit('/admin/animemovie/1/delete')
                ->assertPathIs('/login');
            });
        }
}

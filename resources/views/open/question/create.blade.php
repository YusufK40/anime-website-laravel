@extends ('layouts.layout')

@section('content')
@if ($errors->any())
<div class="bg-red-200 text-red-900 rounded-lg shadow-md p-6 pr-10 mb-8 ">
    <ul class="mt-3 list-disc list-inside text-sn text-red-600">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if (session('status'))
<div class="bg-green-200 text-green-900 rouned-lg shadow-md p-6 pr-10 mb-8">
    {{ session('status') }}
</div>
@endif
    <div class="flex bg-purple-700 p-4 border">
        <h1 class="text-2xl font-semibold text-gray-700 dark:text-gray-200">Request Anime</h1>
    </div>
    <div class="p-10 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-1 xl:grid-cols-1 gap-5 border">
        <form action="{{route('question.store')}}" method="POST">
            @csrf
            <div class="mx-auto w-11/12 md:w-2/3 max-w-lg">
                <div class="relative py-8 px-5 md:px-10 bg-white shadow-md rounded border border-gray-400">
                    <div class="w-full flex justify-start text-gray-700 mb-3">
                        <svg class="icon icon-tabler icon-tabler-wallet m-auto" width="52" height="52" viewBox="0 0 24 24" stroke-width="1" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                        </svg>
                    </div>
                    <h1 class="text-gray-700 font-lg font-bold leading-tight mb-4 text-center">Can't find your Anime? Request it to us!</h1>

                    <label for="name" class="text-gray-600 text-sm font-bold leading-tight">Username</label>
                    <input id="name" name="name" disabled placeholder="James" value="{{ Auth::user()->name }}"
                    class="mb-5 mt-2 text-gray-600 focus:outline-none focus:border focus:border-indigo-700 
                    font-normal w-full h-10 flex items-center pl-3 text-sm border-gray-300 rounded border cursor-not-allowed">

                    <label for="genrename" class="text-gray-600 text-sm font-bold leading-tight">Select a genre</label>
                    <select id="name" name="genre_id" placeholder="Select a genre"
                    class="mb-5 mt-2 text-gray-600 focus:outline-none focus:border focus:border-indigo-700 
                    font-normal w-full h-10 flex items-center pl-3 text-sm border-gray-300 rounded border">
                        @foreach ($genres as $genre)
                            <option value="{{$genre->id}}">{{$genre->genrename}}</option>
                        @endforeach
                    </select>

                    <label for="email2" class="text-gray-600 text-sm font-bold leading-tight">Animeserie</label>
                    <input id="name" name="animetitle" placeholder="Enter your anime here"
                    class="mb-5 mt-2 text-gray-600 focus:outline-none focus:border focus:border-indigo-700 
                    font-normal w-full h-10 flex items-center pl-3 text-sm border-gray-300 rounded border">

                    <div class="flex items-center justify-start w-full">
                        <button class="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-700 transition duration-150 ease-in-out hover:bg-indigo-600 bg-indigo-700 rounded text-white px-8 py-2 text-sm">Send!</button>
                        <a href="{{route('open.animeserie.index')}}" class="focus:outline-none focus:ring-2 focus:ring-offset-2  focus:ring-gray-400 ml-3 bg-gray-100 transition duration-150 text-gray-600 ease-in-out hover:border-gray-400 hover:bg-gray-300 border rounded px-8 py-2 text-sm">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection